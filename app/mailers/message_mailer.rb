class MessageMailer < ApplicationMailer
  def send_message(message)
    @message = message
    mail(from: @message.user.email, to: @message.get_recipients_formatted_to_send, subject: @message.subject)
  end
end
