class Message < ApplicationRecord
	belongs_to :user
    has_many :recipients, class_name: "User"

    after_save :send_email

    def get_recipients_formatted_to_send
      return "" if recipients.nil?
      recipients.map(&:email).join(', ')
    end

    private

    def send_email
      MessageMailer.send_message(self).deliver
    end
end
