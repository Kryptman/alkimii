class DashboardController < ApplicationController
  before_action :authenticate_user!

  def index
  	@emails = User.all.map(&:email)
  	@message = Message.new
  end

  def create
  	message = current_user.messages.build(body: params["message"]["body"], subject: params["message"]["subject"]) unless params["message"].blank?
    recipients = []
    recipients = get_users_from_emails(params["to"]) if params["to"]

    recipients.each do |recipient|
      message.recipients << recipient
    end

    message.save
  	redirect_to dashboard_index_path
  end

  private

  def get_users_from_emails(emails)
    recipients = []

    emails.split(', ').each do |email|
      user = User.find_by_email(email)
      if user.nil?
        user = User.new(email: email)
        user.save(validate: false) 
      end
      recipients << user 
    end
    recipients
  end
end
