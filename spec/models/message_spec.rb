require 'rails_helper'

RSpec.describe Message, type: :model do
  
  before(:each) do
    MessageMailer.stub_chain(:send_message, :deliver)
  end
  it "should belong to a user" do
  	sender = FactoryGirl.create(:user)
  	expect(sender.messages.create).to be
  end

  it "should have a list of recipients" do
  	sender = FactoryGirl.create(:user)
  	recipient1 = FactoryGirl.create(:user)
  	recipient2 = FactoryGirl.create(:user)

  	message = sender.messages.create(body: Faker::Lorem.sentence, subject: Faker::Lorem.sentence)
  	message.recipients << recipient1
  	message.recipients << recipient2

  	expect(message.save).to be
  end

  it "get_recipients_formatted_to_send should return the proper list for the mailer" do
    sender = FactoryGirl.create(:user)
    recipient1 = FactoryGirl.create(:user)
    recipient2 = FactoryGirl.create(:user)

    message = sender.messages.create(body: Faker::Lorem.sentence, subject: Faker::Lorem.sentence)
    message.recipients << recipient1
    message.recipients << recipient2

    expect(message.get_recipients_formatted_to_send).to eq("#{recipient1.email}, #{recipient2.email}")

  end
end
