require 'rails_helper'

RSpec.describe DashboardController, type: :controller do

  describe "GET #index" do
 	login_user

    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end

  	it "should have a current_user" do
      expect(subject.current_user).to_not eq(nil)
  	end
  end

  describe "POST #create" do
 	login_user

    it "returns http success" do       
      params = {
        "to": "#{Faker::Internet.email}, #{Faker::Internet.email}",
        "message": { "subject": Faker::Lorem.sentence,
                     "body": Faker::Lorem.paragraphs
                   }
      }    
      post :create, params: params
      expect(response).to have_http_status(302)
    end

    it "should create a message if the information is OK" do
      params = {
        "to": "#{Faker::Internet.email}, #{Faker::Internet.email}",
        "message": { "subject": Faker::Lorem.sentence,
                     "body": Faker::Lorem.paragraphs
                   }
      }
      expect { post :create, params: params }.to change(Message, :count).by(1)
    end

    it "should create the users if they are not in the database" do
      params = {
        "to": "#{Faker::Internet.email}, #{Faker::Internet.email}",
        "message": { "subject": Faker::Lorem.sentence,
                     "body": Faker::Lorem.paragraphs
                   }
      }
      expect { post :create, params: params }.to change(User, :count).by(2)
    end
  end
end
