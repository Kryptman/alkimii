# Mailing app

## Requirements:

It has been develop using Rails 5.0.2 and Ruby 2.3.3, using Posgresql

### Installation

Run bundle install

After that

rake db:create db:migrate db:seed

And run the server:

rails s -p3000

Everything should be working. For Mailgun, you will require an invitation to receive emails.
